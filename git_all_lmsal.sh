#!/bin/bash

# ADD in your .cshrc 
# setenv LMSALHUB $HOME"/path/LMSAL_HUB"

IRIS_LMSALPY=$LMSALHUB'/iris_hub/iris_lmsalpy/'
lmsaldescription=$LMSALHUB'/lmsal-hub-description/'
os_tools=$LMSALHUB'/miscellaneous/mis-py/os_tools/'
saveidl=$LMSALHUB'/miscellaneous/mis-py/save-like-idl/'
vistools=$LMSALHUB'/miscellaneous/mis-py/vis_tools/'
museguidelines=$LMSALHUB'/muse_hub/muse-hub-guidelines/'
muse_py=$LMSALHUB'/muse_hub/muse_py/'
musefiles=$LMSALHUB'/muse_hub/muse_files/'
museidl=$LMSALHUB'/muse_hub/muse_idl/'
br_cuda=$LMSALHUB'/sims_hub/br_hub/br_cuda/'
br_py=$LMSALHUB'/sims_hub/br_hub/br_py/'
br_intcu=$LMSALHUB'/sims_hub/br_hub/br_intcu/'
br_topocu=$LMSALHUB'/sims_hub/br_hub/br_topocu/'
atompy=$LMSALHUB'/sims_hub/eb_hub/atom_py/'

#lists=($IRIS_LMSALPY $lmsaldescription $os_tools $saveidl $vistools $museguidelines $muse_py $musefiles $museidl $br_cuda $br_py $br_intcu $br_topocu $atompy)
lists=($IRIS_LMSALPY $lmsaldescription $os_tools $saveidl $vistools $br_cuda $br_py $br_intcu $br_topocu $atompy)

for ilist in "${lists[@]}"
do 
    echo "$ilist"
    cd "$ilist"
    echo '####'"$ilist"' ####'
    git $1 
done

