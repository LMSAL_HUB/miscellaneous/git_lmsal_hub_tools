# git_lmsal_hub_tools

Some handy, but still rudimentary, git tools which allow to check, pull, install, etc through all the LMSAL_HUB tree. 

# Dependencies

These scripts are written in cshell.

# Installation

clone: 

       git clone https://gitlab.com/LMSAL_HUB/miscellaneous/git_lmsal_hub_tools

Add in your .cshrc 

     setenv LMSALHUB $HOME"/path/LMSAL_HUB"

Eddit the list of libraries that you are interested to handle in the .sh files. 
Include the .sh files in your command path and give executable permission. 

# How to run: 

## Install all repositories

In order to install all the interested repositories: 

### Non-root install

If you do not have write permission to your Python packages directory, use the following option with setup.py:

      lmsal_install pathname "install --user"

This will download, and install all the python respositories listed in lmsal_install.sh under your home directory (typically ~/.local).

## Developer install

If you want to install and also actively change the codes or contribute to its development, it is recommended that you do a developer install instead:

      lmsal_install pathname develop

This will set up the package such as the source files used are from the git repository that you cloned (only a link to it is placed on the Python packages directory). Can also be combined with the --user flag for local installs.

      lmsal_install pathname "develop --user"

## Check or other git commands through all lmsal_hum repositories. 

You can run simple git commands, e.g., pull in each LMSAL_HUB repository: 

     git_all_lmsal.sh pull

Similarly, one can check status in all of them: 

        git_all_lmsal.sh "status -uno"



