#!/bin/bash
# LMSAL_install pathname develop
# or 
# LMSAL_install pathname install
# or, e.g., 
# LMSAL_install pathname "develop --user"

# LMSALHUB="/yourpath/LMSAL_HUB"
# Add setenv LMSALHUB path in your .cshrc
LMSALHUB=$1

if [ -f $LMSALHUB/sims_hub/eb_hub/atom_py/.git/config ]
then 
    echo '#### ATOM_PY exists:'$LMSALHUB'/sims_hub/eb_hub/atom_py/ ####'
else
    echo '#### INSTALLING ATOM_PY:'$LMSALHUB'/sims_hub/eb_hub/atom_py/ ####'
    if [ -d $LMSALHUB/sims_hub/eb_hub ] 
    then 
	echo "folder exists" 
    else 
	mkdir -p $LMSALHUB/sims_hub/eb_hub
    fi
    cd  $LMSALHUB/sims_hub/eb_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py
    cd atom_py
    python setup.py $2 
fi

if [ -f $LMSALHUB/sims_hub/br_hub/br_cuda/.git/config ]
then 
    echo '#### BR_CUDA exists:'$LMSALHUB'/sims_hub/br_hub/br_cuda/ ####'
else
    echo '#### INSTALLING BR_CUDA:'$LMSALHUB'/sims_hub/br_hub/br_cuda ####'
    if [ -d $LMSALHUB/sims_hub/br_hub ] 
    then 
	echo "folder exists" 
    else
	mkdir -p $LMSALHUB/sims_hub/br_hub
    fi
    cd  $LMSALHUB/sims_hub/br_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_cuda
fi

if [ -f $LMSALHUB/sims_hub/br_hub/br_intcu/.git/config ]
then 
    echo '#### BR_INTCU exists:'$LMSALHUB'/sims_hub/br_hub/br_intcu/ ####'
else
    echo '#### INSTALLING BR_INTCU:'$LMSALHUB'/sims_hub/br_hub/br_intcu ####'
    if [ -d $LMSALHUB/sims_hub/br_hub ] 
    then 
	echo "folder exists" 
    else
	mkdir -p $LMSALHUB/sims_hub/br_hub
    fi
    cd  $LMSALHUB/sims_hub/br_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_intcu
    cd br_intcu
    python setup.py $2
fi

if [ -f $LMSALHUB/sims_hub/br_hub/br_py/.git/config ]
then 
    echo '#### BR_PY exists:'$LMSALHUB'/sims_hub/br_hub/br_py/ ####'
else
    echo '#### INSTALLING BR_PY:'$LMSALHUB'/sims_hub/br_hub/br_py ####'
    if [ -d $LMSALHUB/sims_hub/br_hub ] 
    then 
	echo "folder exists" 
    else 
	mkdir -p $LMSALHUB/sims_hub/br_hub
    fi
    cd  $LMSALHUB/sims_hub/br_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_py
    cd br_py
    python setup.py $2 
fi

if [ -f $LMSALHUB/sims_hub/br_hub/br_topocu/.git/config ]
then 
    echo '#### BR_TOPOCU exists:'$LMSALHUB'/sims_hub/br_hub/br_topocu/ ####'
else
    echo '#### INSTALLING BR_TOPOCU:'$LMSALHUB'/sims_hub/br_hub/br_topocu ####'
    if [ -d $LMSALHUB/sims_hub/br_hub ]
    then 
	echo "folder exists"
    else 
	mkdir -p $LMSALHUB/sims_hub/br_hub
    fi
    cd  $LMSALHUB/sims_hub/br_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_topocu
    cd br_topocu
    python setup.py $2 
fi

if [ -f $LMSALHUB/sims_hub/eb_hub/eb_sswidl/.git/config ]
then 
    echo '#### EB_SSWIDL exists:'$LMSALHUB'/sims_hub/eb_hub/eb_sswidl/ ####'
else
    echo '#### INSTALLING EB_SSWIDL'$LMSALHUB'/sims_hub/eb_hub/eb_sswidl ####'
    if [ -d $LMSALHUB/sims_hub/eb_hub ] 
    then 
	echo "folder exists" 
    else
	mkdir -p $LMSALHUB/sims_hub/eb_hub
    fi
    cd  $LMSALHUB/sims_hub/eb_hub
    git clone https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/eb_sswidl
fi

if [ -f $LMSALHUB/iris_hub/iris_lmsalpy/.git/config ] 
then 
    echo '#### IRIS_PY exist:'$LMSALHUB'/iris_hub/iris_lmsalpy/ ####'
else
    echo '#### INSTALLING IRIS_PY:'$LMSALHUB'/iris_hub/iris_lmsalpy/ ####'
    if [ -d $LMSALHUB/iris_hub ] 
    then
	echo "folder exists" 
    else 
	mkdir -p $LMSALHUB/iris_hub/
    fi
    cd  $LMSALHUB/iris_hub/
    git clone https://gitlab.com/LMSAL_HUB/iris_hub/iris_lmsalpy 
    cd iris_lmsalpy 
    python setup.py $2 
fi

if [ -f $LMSALHUB/lmsal-hub-description/.git/config ]
then 
    echo '#### LMSAL-HUB-DESCRIPTION exists:'$LMSALHUB'/lmsal-hub-description/ ####'
else
    echo '#### INSTALLING LMSAL-HUB-DESCRIPTION:'$LMSALHUB'/lmsal-hub-description/ ####'
    cd  $LMSALHUB
    git clone https://gitlab.com/LMSAL_HUB/lmsal-hub-description
fi


#if [ -f $LMSALHUB/muse_hub/muse-hub-guidelines/.git/config ]
#then 
#    echo '#### MUSE-HUB-GUIDELINES exists:'$LMSALHUB'/muse_hub/muse-hub-guidelines/ ####'
#else
#    echo '#### INSTALLING  MUSE-HUB-GUIDELINES:'$LMSALHUB'/muse_hub/muse-hub-guidelines/ ####'
#    if [ -d $LMSALHUB/muse_hub ] 
#    then
#	echo "folder exists" 
#    else 
#	mkdir -p $LMSALHUB/muse_hub
#    fi
#    cd $LMSALHUB/muse_hub/
#    git clone https://gitlab.com/LMSAL_HUB/muse_hub/muse-hub-guidelines
#fi

#if [ -f $LMSALHUB/muse_hub/muse_files/.git/config ] 
#then 
#    echo '#### MUSE_FILES exists:'$LMSALHUB'/muse_hub/muse_files/ ####'
#else
#    echo '#### INSTALLING  MUSE-FILES:'$LMSALHUB'/muse_hub/muse_files/ ####'
#    if [ -d $LMSALHUB/muse_hub ]
#    then 
#	echo "folder exists" 
#    else
#	mkdir -p $LMSALHUB/muse_hub
#    fi
#    cd $LMSALHUB/muse_hub/
#    git clone https://gitlab.com/LMSAL_HUB/muse_hub/muse_files
#fi 

#if [ -f $LMSALHUB/muse_hub/muse_idl/.git/config ]
#then 
#    echo '#### MUSE_IDL exists:'$LMSALHUB'/muse_hub/muse_idl/ ####'
#else
#    echo '#### INSTALLING  MUSE-IDL:'$LMSALHUB'/muse_hub/muse-idl/ ####'
#    if [ -d $LMSALHUB/muse_hub ] 
#    then 
#	echo "folder exists" 
#    else 
#	mkdir -p $LMSALHUB/muse_hub
#    fi
#    cd $LMSALHUB/muse_hub/
#    git clone https://gitlab.com/LMSAL_HUB/muse_hub/muse_idl
#fi 

#if [ -f $LMSALHUB/muse_hub/muse_py/.git/config ]
#then 
#    echo '#### MUSE-FILES exists:'$LMSALHUB'/muse_hub/muse_py/ ####'
#else
#    echo '#### INSTALLING  MUSE-FILES:'$LMSALHUB'/muse_hub/muse_py/ ####'
#    if [ -d $LMSALHUB/muse_hub ] 
#    then
#	echo "folder exists" 
#    else
#	mkdir -p $LMSALHUB/muse_hub
#    fi
#    cd $LMSALHUB/muse_hub/
#    git clone https://gitlab.com/LMSAL_HUB/muse_hub/muse_py
#    cd muse_py
#    python setup.py $2 
#fi 

if [ -f $LMSALHUB/miscellaneous/mis-py/os_tools/.git/config ]
then 
    echo '#### OS_TOOLS exists:'$LMSALHUB'/miscellaneous/mis-py/os_tools/ ####'
else
    echo '#### INSTALLING OS_TOOLS:'$LMSALHUB'/miscellaneous/mis-py/os_tools/ ####'
    if [ -d $LMSALHUB//miscellaneous/mis-py ] 
    then
	echo "folder exists" 
    else
	mkdir -p $LMSALHUB//miscellaneous/mis-py/
    fi
    cd $LMSALHUB//miscellaneous/mis-py/
    git clone https://gitlab.com/LMSAL_HUB/miscellaneous/mis-py/os_tools
    cd os_tools
    python setup.py $2 
fi  
 
if [ -f $LMSALHUB/miscellaneous/mis-py/save-like-idl/.git/config ]
then 
    echo '#### SAVE-LIKE-IDL exists:'$LMSALHUB'/miscellaneous/mis-py/save-like-idl/ ####'
else
    echo '#### INSTALLING SAVE-LIKE-IDL:'$LMSALHUB'/miscellaneous/mis-py/save-like-idl/ ####'
    if [ -d $LMSALHUB//miscellaneous/mis-py ] 
    then
	echo "folder exists" 
    else 
	mkdir -p $LMSALHUB//miscellaneous/mis-py/
    fi
    cd $LMSALHUB//miscellaneous/mis-py/
    git clone https://gitlab.com/LMSAL_HUB/miscellaneous/mis-py/save-like-idl
    cd save-like-idl
    python setup.py $2  
fi

if [ -f $LMSALHUB/miscellaneous/mis-py/vis_tools/.git/config ]
then 
    echo '#### VIS_TOOLS exists:'$LMSALHUB'/miscellaneous/mis-py/VIS_TOOLS/ ####'
else
    echo '#### INSTALLING VIS_TOOLS:'$LMSALHUB'/miscellaneous/mis-py/vis_tools/ ####'
    if [ -d $LMSALHUB//miscellaneous/mis-py ] 
    then
	echo "folder exists" 
    else
	mkdir -p $LMSALHUB//miscellaneous/mis-py/
    fi
    cd $LMSALHUB//miscellaneous/mis-py/
    git clone https://gitlab.com/LMSAL_HUB/miscellaneous/mis-py/vis_tools
    cd vis_tools
    python setup.py $2 
fi
